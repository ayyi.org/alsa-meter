Ayyi Alsa Meter
---------------

Ayyi Alsa Meter is an Alsa scope plugin providing audio metering for Linux
users that are not using Jack or Pulse.

It is intended to be used with a separate application providing the UI. This
fixes the issue with other scope plugins of a new window being created each
time a new track is played. The plugin sends the level information to the
UI for display.

To use it, create a new device in asound.conf and configure your
player to output to this device. See below for configuration details.


### Requirements

	- alsa
	- dbus-glib-1

### Building

```
	/autogen.sh  # skip this if building from tarball
	./configure
	make
	su -c "make install"
```

3) Add the following to ~/.asoundrc

```
pcm_scope.ayyimeter {
	type ayyimeter
}
pcm_scope_type.ayyimeter {
	lib /usr/local/lib/libayyimeter.so
}
pcm.ayyimeter {
	type meter
	slave.pcm 'hw:0,0'
	frequency 50
	scopes.0 ayyimeter
}
pcm.dsp0 ayyimeter
```

### Usage

```
aplay -D ayyimeter sound.wav
arecord -D ayyimeter soundrec.wav

mplayer -ao alsa:device=ayyimeter song.flac
```

Note the meter is currently stereo only.
