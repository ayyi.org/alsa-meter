/**
* +----------------------------------------------------------------------+
* | This file is part of the Ayyi project. http://ayyi.org               |
* | copyright (C) 2010-2020 Tim Orford <tim@orford.org>                  |
* +----------------------------------------------------------------------+
* | This program is free software; you can redistribute it and/or modify |
* | it under the terms of the GNU General Public License version 3       |
* | as published by the Free Software Foundation.                        |
* +----------------------------------------------------------------------+
*
*/
#include <gio/gio.h>
#include "debug/debug.h"

struct _p2p
{
	GDBusConnection* connection;
} p2p = {0,};


static void
setup_peer_comms (GError** error)
{
	p2p.connection = g_dbus_connection_new_for_address_sync ("unix:abstract=meter", G_DBUS_CONNECTION_FLAGS_AUTHENTICATION_CLIENT, NULL, NULL, error);

	if(*error){
		return;
	}

#if 0
	if(p2p.connection){
		g_print ("Connected.\n" "Negotiated capabilities: unix-fd-passing=%d\n", g_dbus_connection_get_capabilities (p2p.connection) & G_DBUS_CAPABILITY_FLAGS_UNIX_FD_PASSING);
	}
#endif
}

