/**
* +----------------------------------------------------------------------+
* | This file is part of the Ayyi project. http://ayyi.org               |
* | copyright (C) 2020-2020 Tim Orford <tim@orford.org>                  |
* +----------------------------------------------------------------------+
* | This program is free software; you can redistribute it and/or modify |
* | it under the terms of the GNU General Public License version 3       |
* | as published by the Free Software Foundation.                        |
* +----------------------------------------------------------------------+
*
*/
#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <math.h>
#include <string.h>
#include <errno.h>
#include <alsa/asoundlib.h>
#include <signal.h>

#include "p2p.c"

#define MAX_METERS 2

#define P_GERR if(error){ perr("%s\n", error->message); g_error_free(error); error = NULL; }

typedef struct
{
	int16_t levelchan;
	int16_t peak;
	unsigned int peak_age;
	int16_t previous;
} snd_pcm_scope_ayyimeter_channel_t;


typedef struct _snd_pcm_scope_ameter
{
	snd_pcm_t* pcm;
	snd_pcm_scope_t* s16;
	snd_pcm_scope_ayyimeter_channel_t* channels;
	snd_pcm_uframes_t old;
	int top;
	unsigned int bar_width;
	unsigned int decay_ms;
} snd_pcm_scope_ayyimeter_t;


int peaks[MAX_METERS];
int ptime[MAX_METERS];
/* float env[MAX_METERS]; */


/* int iec_scale(float db) { */
/*          float def = 0.0f; */

/*          if (db < -70.0f) { */
/*                  def = 0.0f; */
/*          } else if (db < -60.0f) { */
/*                  def = (db + 70.0f) * 0.25f; */
/*          } else if (db < -50.0f) { */
/*                  def = (db + 60.0f) * 0.5f + 5.0f; */
/*          } else if (db < -40.0f) { */
/*                  def = (db + 50.0f) * 0.75f + 7.5; */
/*          } else if (db < -30.0f) { */
/*                  def = (db + 40.0f) * 1.5f + 15.0f; */
/*          } else if (db < -20.0f) { */
/*                  def = (db + 30.0f) * 2.0f + 30.0f; */
/*          } else if (db < 0.0f) { */
/*                  def = (db + 20.0f) * 2.5f + 50.0f; */
/*          } else { */
/*                  def = 100.0f; */
/*          } */

/*          return (int)(def * 2.0f); */
/* } */


static int
level_enable (snd_pcm_scope_t* scope)
{
	snd_pcm_scope_ayyimeter_t* level = snd_pcm_scope_get_callback_private (scope);
	level->channels = calloc (snd_pcm_meter_get_channels (level->pcm), sizeof (*level->channels));
	if (!level->channels) {
		if (level)
			free (level);
		return -ENOMEM;
	}

	snd_pcm_scope_set_callback_private (scope, level);

	return 0;
}


static void
level_disable (snd_pcm_scope_t* scope)
{
	snd_pcm_scope_ayyimeter_t* level = snd_pcm_scope_get_callback_private (scope);

	if (level->channels)
		free (level->channels);
}


static void
level_close (snd_pcm_scope_t* scope)
{
	snd_pcm_scope_ayyimeter_t* level = snd_pcm_scope_get_callback_private (scope);
	if (level)
		free (level);
}


static void
level_start (snd_pcm_scope_t* scope ATTRIBUTE_UNUSED)
{
	sigset_t s;
	sigemptyset (&s);
	sigaddset (&s, SIGINT);
	pthread_sigmask (SIG_BLOCK, &s, NULL);
}


static void
level_stop (snd_pcm_scope_t* scope ATTRIBUTE_UNUSED)
{
}


static void
level_update (snd_pcm_scope_t* scope)
{
	snd_pcm_scope_ayyimeter_t* level = snd_pcm_scope_get_callback_private (scope);
	snd_pcm_t* pcm = level->pcm;

	snd_pcm_sframes_t size = snd_pcm_meter_get_now (pcm) - level->old;
	if (size < 0)
		size += snd_pcm_meter_get_boundary (pcm);
	snd_pcm_uframes_t offset = level->old % snd_pcm_meter_get_bufsize (pcm);
	snd_pcm_uframes_t cont = snd_pcm_meter_get_bufsize (pcm) - offset;
	snd_pcm_uframes_t size1 = size;
	if (size1 > cont)
		size1 = cont;
	snd_pcm_uframes_t size2 = size - size1;

	unsigned int channels = snd_pcm_meter_get_channels (pcm);
	for (unsigned int c = 0; c < channels; c++) {
		int s, lev = 0;
		snd_pcm_scope_ayyimeter_channel_t* l = &level->channels[c];
		int16_t* ptr = snd_pcm_scope_s16_get_channel_buffer (level->s16, c) + offset;
		for (snd_pcm_uframes_t n = size1; n > 0; n--) {
			s = *ptr;
			if (s < 0)
				s = -s;
			if (s > lev)
				lev = s;
			ptr++;
		}
		ptr = snd_pcm_scope_s16_get_channel_buffer (level->s16, c);
		for (snd_pcm_uframes_t n = size2; n > 0; n--) {
			s = *ptr;
			if (s < 0)
				s = -s;
			if (s > lev)
				lev = s;
			ptr++;
		}

		l->levelchan = lev;
		l->previous = lev;
	}

	level->old = snd_pcm_meter_get_now (pcm);

	//#define to_log(L) (log10(L) / 1. + 1.)
	//#define to_log(L) (log2(L) / 8. + 1.)
	#define to_log(L) (log(L) / 4. + 1.) // base e

#if 0
	#define DBUS_TIMEOUT 10
	GError* e;
	g_dbus_connection_call_sync (
		p2p.connection,
		NULL,                 // bus_name
		"/",
		"org.ayyi.alsameter", // interface name
		"level",
		g_variant_new ("(dd)", (double)to_log(level->channels[0].levelchan / 32767.), (double)to_log(level->channels[1].levelchan / 32767.)),
		NULL, // return
		G_DBUS_CALL_FLAGS_NONE,
		DBUS_TIMEOUT,
		NULL,
		&e
	);
	if(e) pwarn("%s", e->message);
#else
	g_dbus_connection_call (
		p2p.connection,
		NULL,                 // bus_name
		"/",
		"org.ayyi.alsameter", // interface name
		"level",
		g_variant_new ("(dd)", (double)to_log(level->channels[0].levelchan / 32767.), (double)to_log(level->channels[1].levelchan / 32767.)),
		NULL,                 // reply type
		G_DBUS_CALL_FLAGS_NONE,
		-1,                   // no timeout
		NULL,
		NULL,                 // no callback, so message will be sent with G_DBUS_MESSAGE_FLAGS_NO_REPLY_EXPECTED
		NULL
	);
#endif
}


static void
level_reset (snd_pcm_scope_t* scope)
{
	snd_pcm_scope_ayyimeter_t* level = snd_pcm_scope_get_callback_private (scope);
	snd_pcm_t* pcm = level->pcm;
	memset (level->channels, 0, snd_pcm_meter_get_channels (pcm) * sizeof (*level->channels));
	level->old = snd_pcm_meter_get_now (pcm);
}


snd_pcm_scope_ops_t level_ops = {
	enable : level_enable,
	disable : level_disable,
	close : level_close,
	start : level_start,
	stop : level_stop,
	update : level_update,
	reset : level_reset,
};


int
snd_pcm_scope_ayyimeter_open (snd_pcm_t* pcm, const char* name, snd_pcm_scope_t** scopep)
{
	GError* error = NULL;
	setup_peer_comms(&error);
	if(error){
		P_GERR;
		return 1;
	}
	g_return_val_if_fail(p2p.connection, 1);

	snd_pcm_scope_t* scope;
	int err = snd_pcm_scope_malloc (&scope);
	if (err < 0)
		return err;

	snd_pcm_scope_ayyimeter_t* level = calloc (1, sizeof(*level));
	if (!level) {
		if (scope)
			free (scope);
		return -ENOMEM;
	}

	level->pcm = pcm;

	snd_pcm_scope_t* s16 = snd_pcm_meter_search_scope (pcm, "s16");
	if (!s16) {
		err = snd_pcm_scope_s16_open (pcm, "s16", &s16);
		if (err < 0) {
			if (scope)
				free (scope);
			if (level)
				free (level);
			return err;
		}
	}
	level->s16 = s16;
	snd_pcm_scope_set_ops (scope, &level_ops);
	snd_pcm_scope_set_callback_private (scope, level);
	if (name)
		snd_pcm_scope_set_name (scope, strdup (name));
	snd_pcm_meter_add_scope (pcm, scope);
	*scopep = scope;

	return 0;
}


int
_snd_pcm_scope_ayyimeter_open (snd_pcm_t* pcm, const char* name, snd_config_t* root ATTRIBUTE_UNUSED, snd_config_t* conf)
{
	snd_pcm_scope_t* scope;

	/*
	 *  Config parameters can be specified in the asound.conf
	 */
	snd_config_iterator_t i, next;
	snd_config_for_each (i, next, conf)
	{
		snd_config_t* n = snd_config_iterator_entry (i);

		const char* id;
		if (snd_config_get_id (n, &id) < 0)
			continue;

		if (!strcmp (id, "comment"))
			continue;

		if (!strcmp (id, "type"))
			continue;

		const char* val;
		if(snd_config_get_string(n, &val) < 0){
			SNDERR ("Invalid type for %s", id);
			return -EINVAL;
		}

		continue;

		SNDERR ("Unknown field %s", id);
		return -EINVAL;
	}

	return snd_pcm_scope_ayyimeter_open (pcm, name, &scope);
}
